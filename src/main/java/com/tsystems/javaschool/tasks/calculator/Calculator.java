package com.tsystems.javaschool.tasks.calculator;

import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.Locale;

public class Calculator {

    public static void main(String[] args) {
        Calculator c = new Calculator();
        System.out.println(c.evaluate("(1+38)*4-5"));
        System.out.println(c.evaluate("7*6/2+8"));
        System.out.println(c.evaluate("-12)1//("));
        System.out.println(c.evaluate("(12*(5-1)"));
        System.out.println(c.evaluate("16**2/4"));
        System.out.println(c.evaluate("4-(6/(7+1))"));

    }

    public String evaluate(String statement) {

        LinkedList<Double> value;
        try {
            String calcExp = statement.replaceAll(" ", "");
            value = new LinkedList<>();
            LinkedList<Character> mathSigns = new LinkedList<>();

            for (int i = 0; i < calcExp.length(); i++) {
                char ch = calcExp.charAt(i);
                if (ch == '(') {
                    mathSigns.add('(');
                } else if (ch == ')') {
                    while (mathSigns.getLast() != '(') {
                        mathOperation(value, mathSigns.removeLast());
                    }
                    mathSigns.removeLast();
                } else if (isOperator(ch)) {
                    while (!mathSigns.isEmpty() && priorityOp(mathSigns.getLast()) >= priorityOp(ch)) {
                        mathOperation(value, mathSigns.removeLast());
                    }
                    mathSigns.add(ch);
                } else if (Character.isDigit(ch)) {
                    String operand = "";
                    while (i < calcExp.length() && (calcExp.charAt(i) == '.' || Character.isDigit(calcExp.charAt(i))
                            || calcExp.charAt(i) == ',')) {
                        if (calcExp.charAt(i) == ',') {
                            return null;
                        }
                        operand += calcExp.charAt(i++);
                    }
                    --i;
                    try {
                        value.add(Double.parseDouble(operand));
                    } catch (NumberFormatException e) {
                        return null;
                    }
                }
            }
            while (!mathSigns.isEmpty()) {
                mathOperation(value, mathSigns.removeLast());
            }
        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }
        try {
            NumberFormat numberFormatter = NumberFormat.getNumberInstance(Locale.US);
            numberFormatter.setMaximumFractionDigits(4);
            numberFormatter.setMinimumFractionDigits(0);
            return numberFormatter.format(value.get(0));

        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }
    }

    public boolean isOperator(char ch) {
        return ch == '+' || ch == '-' || ch == '*' || ch == '/';
    }

    public int priorityOp(char operator) {
        if (operator == '*' || operator == '/') {
            return 1;
        } else if (operator == '+' || operator == '-') {
            return 0;
        } else {
            return -1;
        }
    }

    public void mathOperation(LinkedList<Double> exp, char operator) throws Exception {
        double one = exp.removeLast();
        double two = exp.removeLast();
        switch (operator) {
            case '+':
                exp.add(two + one);
                break;
            case '-':
                exp.add(two - one);
                break;
            case '*':
                exp.add(two * one);
                break;
            case '/':
                if (one == 0) {
                    throw new ArithmeticException();
                }
                exp.add(two / one);
                break;
        }
    }
}