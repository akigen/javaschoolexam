package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;


public class DuplicateFinder {
    public boolean process(File sourceFile, File targetFile) throws IllegalArgumentException {

        if (sourceFile == null || targetFile == null) {
            throw new IllegalArgumentException();
        }

        try {
            BufferedReader inputFile = new BufferedReader(new FileReader(sourceFile));
            BufferedWriter outputFile = new BufferedWriter(new FileWriter(targetFile));

            Map<String, Integer> mapInputFile = new TreeMap<String, Integer>();
            String line;
            while ((line = inputFile.readLine()) != null) {
                if (mapInputFile.containsKey(line)) {
                    mapInputFile.put(line, mapInputFile.get(line) + 1);
                } else {
                    mapInputFile.put(line, 1);
                }
            }
            for (Map.Entry<String, Integer> entry : mapInputFile.entrySet()) {

                outputFile.write(entry.getKey() + " [" + entry.getValue() + "]");
                outputFile.newLine();
            }

            inputFile.close();
            outputFile.close();

            return true;

        } catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }
}
