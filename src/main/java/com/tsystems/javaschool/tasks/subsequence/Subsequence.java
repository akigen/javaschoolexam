package com.tsystems.javaschool.tasks.subsequence;

import java.util.Arrays;
import java.util.List;

public class Subsequence {

    public static void main(String[] args) {
        Subsequence s = new Subsequence();
        boolean b = s.find(Arrays.asList("A", "B", "C", "D"),
                Arrays.asList("BD", "A", "ABC", "B", "M", "D", "M", "C", "DC", "D"));
        System.out.println(b); // Result: true
    }


    public boolean find(List x, List y) {

        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        if (x.size() == 0) {
            return true;
        }

        int countOfEqualsObj = 0;
        int n = 0;
        boolean result = false;
        for (int i = 0; i < x.size(); i++) {
            if (y.contains(x.get(i))) {
                for (int j = n; j < y.size(); j++) {
                    if (x.get(i).equals(y.get(j))) {
                        countOfEqualsObj++;
                        n = j + 1;
                        if (x.size() == countOfEqualsObj) {
                            return true;
                        }
                        break;
                    }
                }
            } else {
                result = false;
            }
        }
        return result;
    }
}
